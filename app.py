import pyperclip
import rumps
from PIL import ImageGrab

from scanner import Scanner


class AwesomeStatusBarApp(rumps.App):
    def __init__(self):
        super().__init__('Chess')
        self.menu = ['FEN W', 'FEN B']

    @rumps.clicked('FEN W')
    def fen_white(self, _):
        self.fen('w')

    @rumps.clicked('FEN B')
    def fen_black(self, _):
        self.fen('b')

    def fen(self, to_move):
        img = ImageGrab.grabclipboard()

        if img is None:
            rumps.notification('Scan failed', 'Cannot load image from clipboard', '')
            return

        scanner = Scanner(img)
        try:
            fen = scanner.get_fen(to_move)
        except Exception as e:
            rumps.notification('Scan failed', 'Exception raised', str(e))
            print(e)
            return

        pyperclip.copy(fen)
        rumps.notification('Scan is complete', 'FEN is copied to the clipboard', fen)


if __name__ == '__main__':
    AwesomeStatusBarApp().run()
