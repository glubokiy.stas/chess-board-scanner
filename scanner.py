import numpy as np
from PIL import ImageEnhance, ImageChops, Image
from PIL.Image import Image as ImageObj
from numpy.lib.stride_tricks import sliding_window_view

from piece_matcher import match_piece, EXPECTED_SQUARE_WIDTH, resize_by_ratio


def trim_bbox(im):
    bg = Image.new(im.mode, im.size, im.getpixel((0, 0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    return bbox


def inc_contrast(im):
    bright_img = ImageEnhance.Brightness(im).enhance(3)
    return ImageEnhance.Contrast(bright_img).enhance(5)


def is_line_black(line, threshold=0.9):
    black_fraction = np.mean(np.where(line < 50, 1, 0))
    return black_fraction > threshold


class Scanner:

    def __init__(self, img: ImageObj):
        self.img = img.convert('L')
        self.edges = []

    def get_fen(self, to_move: str):
        """Return FEN string of the position.

        Args:
            to_move: 'w' or 'b' (white or black)
        """
        self._trim_image()
        self._find_edges()
        self._crop_board()
        self._find_biggest_square()
        self._find_size()
        self._cut_board_into_squares()
        self._match_pieces()
        return self._load_fen(to_move)

    def _trim_image(self):
        bbox = trim_bbox(inc_contrast(self.img))
        self.trimmed_img = self.img.crop(bbox)

    def _find_edges(self):
        arr = np.array(inc_contrast(self.trimmed_img))

        slice_all = slice(None)

        for edge, sl in [('t', (0, slice_all)), ('r', (slice_all, -1)), ('b', (-1, slice_all)), ('l', (slice_all, 0))]:
            if is_line_black(arr[sl]):
                self.edges.append(edge)

        if len(self.edges) < 2:
            raise ValueError('At least two edges must be on the board')

    def _crop_board(self):
        arr = np.array(inc_contrast(self.trimmed_img))
        height, width = arr.shape

        # Find vertical inner edges
        vertical_black_lines = []
        for i, col in enumerate(arr.T):
            if is_line_black(col):
                vertical_black_lines.append(i)

        lines_on_left = [i for i in vertical_black_lines if i < width // 2]
        left = max(lines_on_left) + 1 if lines_on_left else 0

        lines_on_right = [i for i in vertical_black_lines if i > width // 2]
        right = min(lines_on_right) - 1 if lines_on_right else width - 1

        # Find horizontal inner edges
        horizontal_black_lines = []
        for i, col in enumerate(arr):
            if is_line_black(col):
                horizontal_black_lines.append(i)

        lines_on_top = [i for i in horizontal_black_lines if i < height // 2]
        top = max(lines_on_top) + 1 if lines_on_top else 0

        lines_on_bottom = [i for i in horizontal_black_lines if i > height // 2]
        bottom = min(lines_on_bottom) - 1 if lines_on_bottom else height - 1

        self.board_img = self.trimmed_img.crop([left, top, right, bottom])

    def _find_biggest_square(self):
        """Find the biggest white square which will help find the size of the board."""
        # No need to do sliding window if opposite edges are present, since it's always
        # 8 squares between opposite edges
        if 't' in self.edges and 'b' in self.edges:
            self.biggest_square_side = self.board_img.height // 8
            return

        if 'l' in self.edges and 'r' in self.edges:
            self.biggest_square_side = self.board_img.width // 8
            return

        self.biggest_square_side = 1
        min_side = min(self.board_img.size)

        arr = np.array(self.board_img)

        for side in range(5, min_side, 5):
            slide = sliding_window_view(arr, window_shape=(side, side))
            for area in (area for area_list in slide for area in area_list):
                if np.all(np.where(area > 200, 1, 0)):
                    self.biggest_square_side = side
                    break
            else:
                # If white square wasn't found for this side size, no point in going further
                break

    def _find_size(self):
        board_height = round(self.board_img.height / self.biggest_square_side)
        board_width = round(self.board_img.width / self.biggest_square_side)

        if board_height > 8 or board_width > 8:
            raise ValueError('The board is too big')

        self.board_size = (board_height, board_width)

        # Resize board to match sample pieces size
        ratio = (EXPECTED_SQUARE_WIDTH * board_width) / self.board_img.width
        self.board_img = resize_by_ratio(self.board_img, ratio, resample=Image.NEAREST)

    def _cut_board_into_squares(self):
        self.squares = [[None for _ in range(8)] for _ in range(8)]

        offset_x = 8 - self.board_size[1] if 'l' not in self.edges else 0
        offset_y = 8 - self.board_size[0] if 't' not in self.edges else 0

        square_width = self.board_img.width / self.board_size[1]
        square_height = self.board_img.height / self.board_size[0]

        for i in range(self.board_size[0]):
            for j in range(self.board_size[1]):
                left = round(square_width * j)
                top = round(square_height * i)
                square_img = self.board_img.crop([left, top, left + square_width, top + square_height])

                self.squares[offset_y + i][offset_x + j] = square_img

    def _match_pieces(self):
        self.pieces = [[None for _ in range(8)] for _ in range(8)]

        for i in range(8):
            for j in range(8):
                square_img = self.squares[i][j]
                if square_img is not None:
                    sq_color = 'w' if (i + j) % 2 == 0 else 'b'
                    self.pieces[i][j] = match_piece(square_img, square_color=sq_color)

    def _load_fen(self, to_move):
        ranks = ['' for _ in range(8)]

        for rank in range(8):
            for i, piece in enumerate(self.pieces[rank]):
                if piece is not None:
                    ranks[rank] += piece
                    continue

                # Handle empty space
                if i == 0:
                    ranks[rank] += '1'
                elif self.pieces[rank][i - 1] is None:
                    # If previous is empty, increase the number
                    ranks[rank] = ranks[rank][:-1] + str(int(ranks[rank][-1]) + 1)
                else:
                    ranks[rank] += '1'

        return '/'.join(ranks) + f' {to_move} - - 0 1'
