from scanner import Scanner
from tests.conftest import run_scanner


def test_cut_8by8(board_8by8):
    s = run_scanner(Scanner(board_8by8), 'cut_board_into_squares')
    assert s.squares[0][0] is not None
    assert s.squares[0][-1] is not None
    assert s.squares[-1][-1] is not None
    assert s.squares[-1][0] is not None


def test_cut_half(board_half):
    s = run_scanner(Scanner(board_half), 'cut_board_into_squares')
    assert s.squares[0][0] is None
    assert s.squares[0][-1] is not None
    assert s.squares[-1][-1] is not None
    assert s.squares[-1][0] is None


def test_cut_corner(board_corner):
    s = run_scanner(Scanner(board_corner), 'cut_board_into_squares')
    assert s.squares[0][0] is None
    assert s.squares[0][-1] is not None
    assert s.squares[-1][-1] is None
    assert s.squares[-1][0] is None
