from piece_matcher import match_piece


def test_matching(square):
    square_img, expected = square
    assert match_piece(square_img) == expected
