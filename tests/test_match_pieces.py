from scanner import Scanner
from tests.conftest import run_scanner


def test_match_pieces_8by8(board_8by8):
    s = run_scanner(Scanner(board_8by8), 'match_pieces')
    assert s.pieces[0][0] == 'r'
    assert s.pieces[0][1] is None
    assert s.pieces[0][2] is None
    assert s.pieces[0][3] == 'r'
    assert s.pieces[1][0] is None
    assert s.pieces[7][4] is None
    assert s.pieces[7][6] == 'K'
    assert s.pieces[7][7] is None


def test_match_pieces_half(board_half):
    s = run_scanner(Scanner(board_half), 'match_pieces')
    assert set(s.pieces[0]) == {None}
    assert set(s.pieces[1]) == {None}
    assert set(s.pieces[4]) == {None}
    assert set(s.pieces[7]) == {None}
    assert s.pieces[2][6] == 'k'
    assert s.pieces[3][7] == 'b'
    assert s.pieces[5][5] == 'r'
    assert s.pieces[6][4] == 'K'
    assert s.pieces[6][7] == 'R'


def test_match_pieces_corner(board_corner):
    s = run_scanner(Scanner(board_corner), 'match_pieces')
    assert s.pieces[0][4] == 'r'
    assert s.pieces[0][5] is None
    assert s.pieces[0][6] == 'n'
    assert s.pieces[0][7] == 'k'
    assert s.pieces[2][6] == 'q'
    assert s.pieces[3][6] == 'N'
    assert s.pieces[4][5] == 'K'
    assert s.pieces[4][7] == 'Q'

