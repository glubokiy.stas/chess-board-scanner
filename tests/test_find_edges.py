from scanner import Scanner
from tests.conftest import run_scanner


def test_edges_8by8(board_8by8):
    s = run_scanner(Scanner(board_8by8), 'find_edges')
    assert set(s.edges) == set('trbl')


def test_edges_half(board_half):
    s = run_scanner(Scanner(board_half), 'find_edges')
    assert set(s.edges) == set('trb')


def test_edges_corner(board_corner):
    s = run_scanner(Scanner(board_corner), 'find_edges')
    assert set(s.edges) == set('tr')
