from scanner import Scanner


def test_8by8(board_8by8):
    s = Scanner(img=board_8by8)
    assert s.get_fen('w') == 'r2r2k1/5qpp/8/3bp3/P1B5/2Q1P1P1/7P/R1R3K1 w - - 0 1'


def test_8by8_2(board_8by8_2):
    s = Scanner(img=board_8by8_2)
    assert s.get_fen('w') == 'r2r2k1/5qpp/8/3bp3/P1B5/2Q1P1P1/7P/R1R3K1 w - - 0 1'


def test_8by8_3(board_8by8_3):
    s = Scanner(img=board_8by8_3)
    assert s.get_fen('w') == '6k1/p2p1rnp/1p1Pp2q/8/1PPQ3P/8/PB3P1K/6R1 w - - 0 1'


def test_8by8_4(board_8by8_4):
    s = Scanner(img=board_8by8_4)
    assert s.get_fen('w') == '4N1k1/8/6KB/4r3/8/8/8/8 w - - 0 1'


def test_8by8_k_on_b(board_8by8_k_on_b):
    s = Scanner(img=board_8by8_k_on_b)
    assert s.get_fen('w') == '4q2k/6p1/7p/4B3/7Q/8/8/8 w - - 0 1'


def test_half(board_half):
    s = Scanner(img=board_half)
    assert s.get_fen('b') == '8/8/6k1/7b/8/5r2/4K2R/8 b - - 0 1'


def test_half_2(board_half_2):
    s = Scanner(img=board_half_2)
    assert s.get_fen('w') == '5b2/7K/6P1/8/4k2B/8/8/8 w - - 0 1'


def test_corner(board_corner):
    s = Scanner(img=board_corner)
    assert s.get_fen('w') == '4r1nk/6pp/6q1/6N1/5K1Q/8/8/8 w - - 0 1'
