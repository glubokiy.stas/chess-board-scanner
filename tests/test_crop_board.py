from scanner import Scanner
from tests.conftest import run_scanner


def test_crop_8by8(board_8by8):
    s = run_scanner(Scanner(board_8by8), 'crop_board')
    assert 14 <= s.trimmed_img.size[0] - s.board_img.size[0] <= 18
    assert 14 <= s.trimmed_img.size[1] - s.board_img.size[1] <= 18


def test_crop_half(board_half):
    s = run_scanner(Scanner(board_half), 'crop_board')
    assert 7 <= s.trimmed_img.size[0] - s.board_img.size[0] <= 9
    assert 14 <= s.trimmed_img.size[1] - s.board_img.size[1] <= 18


def test_crop_corner(board_corner):
    s = run_scanner(Scanner(board_corner), 'crop_board')
    assert 7 <= s.trimmed_img.size[0] - s.board_img.size[0] <= 9
    assert 7 <= s.trimmed_img.size[1] - s.board_img.size[1] <= 9
