from pathlib import Path

import pytest
from PIL import Image


@pytest.fixture
def board_8by8():
    return Image.open(Path(__file__).parent / 'boards' / '8by8.jpg')


@pytest.fixture
def board_8by8_2():
    return Image.open(Path(__file__).parent / 'boards' / '8by8_2.jpeg')


@pytest.fixture
def board_8by8_3():
    return Image.open(Path(__file__).parent / 'boards' / '8by8_3.jpeg')


@pytest.fixture
def board_8by8_4():
    return Image.open(Path(__file__).parent / 'boards' / '8by8_4.jpeg')


@pytest.fixture
def board_8by8_k_on_b():
    return Image.open(Path(__file__).parent / 'boards' / '8by8_k_on_b.jpeg')


@pytest.fixture
def board_half():
    return Image.open(Path(__file__).parent / 'boards' / 'half.jpeg')


@pytest.fixture
def board_half_2():
    return Image.open(Path(__file__).parent / 'boards' / 'half_2.jpeg')


@pytest.fixture
def board_corner():
    return Image.open(Path(__file__).parent / 'boards' / 'corner.jpeg')


def get_square_id(square_path):
    return square_path.name.replace('.png', '')


@pytest.fixture(params=list((Path(__file__).parent / 'squares').glob('*.png')), ids=get_square_id)
def square(request):
    square_path = request.param
    filename = square_path.name
    if '_on_' in filename:
        expected = filename[0]
    else:
        expected = None
    return Image.open(square_path), expected


def run_scanner(scanner, method=None):
    methods = [
        'trim_image',
        'find_edges',
        'crop_board',
        'find_biggest_square',
        'find_size',
        'cut_board_into_squares',
        'match_pieces',
    ]

    for m in methods:
        getattr(scanner, f'_{m}')()
        if method == m:
            break

    return scanner
