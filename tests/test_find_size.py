from scanner import Scanner
from tests.conftest import run_scanner


def test_find_size_8by8(board_8by8):
    s = run_scanner(Scanner(board_8by8), 'find_size')
    assert s.board_size == (8, 8)


def test_find_size_half(board_half):
    s = run_scanner(Scanner(board_half), 'find_size')
    assert s.board_size == (8, 4)


def test_find_size_corner(board_corner):
    s = run_scanner(Scanner(board_corner), 'find_size')
    assert s.board_size == (5, 4)
