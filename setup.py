from pathlib import Path

from setuptools import setup

PIECES_DIR = Path(__file__) / 'pieces'

APP = ['app.py']
DATA_FILES = [p.as_posix() for p in PIECES_DIR.rglob('*.png')]

OPTIONS = {
    'argv_emulation': True,
    'iconfile': 'icon.icns',
    'plist': {
        'CFBundleShortVersionString': '0.2.0',
        'LSUIElement': True,
    },
    'packages': ['rumps', 'pyperclip', 'PIL', 'numpy'],
    'resources': ['./pieces']
}

setup(
    app=APP,
    name='Chess Board Scanner',
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'], install_requires=['rumps', 'pyperclip', 'Pillow', 'numpy'],
)
