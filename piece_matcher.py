from functools import cache
from pathlib import Path

import numpy as np
from PIL import Image
from numpy.lib.stride_tricks import sliding_window_view

PIECES = list('BKNPQRbknpqr')
BASE_DIR = Path('./pieces')


# Width of the square from which sample pieces were taken
ORIGINAL_SQUARE_WIDTH = 217

# Shrinking is important to reduce the number of sliding windows.
# If shrink too much, the accuracy drops
# If shrink too little, performance is poor (about 1s to match 1 square)
SHRINK_FACTOR = 4
EXPECTED_SQUARE_WIDTH = round(ORIGINAL_SQUARE_WIDTH / SHRINK_FACTOR)


@cache
def get_pieces_images():
    result = {}
    for piece in PIECES:
        folder = 'white' if piece.isupper() else 'black'
        piece_img = Image.open(BASE_DIR / folder / f'{piece}.png').convert('LA')

        # Remove several rows of pixels for more robust sliding window
        piece_img = piece_img.crop((0, 0, piece_img.width, piece_img.height - 7))

        result[piece] = resize_by_ratio(piece_img, ratio=1 / SHRINK_FACTOR)

    return result


def match_piece(square_img, square_color=None):
    square_img = square_img.convert('L')
    square_img = Image.fromarray(((np.array(square_img) > 210) * 255).astype(np.uint8))

    # Resize square so that a piece on it is equal size to the sample piece
    # (this reduces the number of sliding windows to check)
    if abs(square_img.width - EXPECTED_SQUARE_WIDTH) > 1:
        ratio = EXPECTED_SQUARE_WIDTH / square_img.width
        square_img = resize_by_ratio(square_img, ratio, resample=Image.NEAREST)

    # Handle empty squares, this is faster and more robust than having a threshold
    # for sliding window approach
    if square_color in [None, 'w'] and is_empty_white_square(square_img):
        return None
    if square_color in [None, 'b'] and is_empty_black_square(square_img):
        return None

    best_score = 0
    best_piece = None

    for piece in PIECES:
        score = prob_piece_on_square(piece=piece, square_img=square_img)[0]
        if score > best_score:
            best_piece = piece
            best_score = score

    return best_piece


def is_empty_black_square(square_img):
    # Only about 25% of empty dark squares are black
    if np.mean(np.array(square_img) < 230) > 0.33:
        return False

    arr = ((np.array(square_img.rotate(45)) > 240) * 255).astype(np.uint8)

    white_lines = []
    for i, v_line in enumerate(arr.T):
        v_line[v_line < 10] = 0
        v_line = np.trim_zeros(v_line)

        if np.all(v_line > 250):
            white_lines.append(i)

    white_stripes = []
    for i in white_lines:
        if all(i - x not in white_stripes for x in range(1, 3)):
            white_stripes.append(i)
        else:
            white_stripes[-1] = i

    return len(white_stripes) > 6


def is_empty_white_square(square_img):
    # Cut away 20% on top, right, left and bottom (like zooming into image a little)
    # and check if it's mostly white. This cropping is needed to ignore artifacts on edges
    x = round(square_img.width * 0.1)
    return np.mean(np.array(square_img)[x:-x, x:-x] > 250) > 0.99


def prob_piece_on_square(piece, square_img):
    piece_img = get_pieces_images()[piece]

    piece_arr = np.array(piece_img).astype(np.int16)
    square_arr = np.array(square_img).astype(np.int16)

    piece_shape = piece_arr.shape[:2]

    best_score = 0
    best_area = None

    # Pieces tend to be placed closer to the bottom right, so we can always ignore checking
    # first couple of rows/columns
    slide = sliding_window_view(square_arr[3:, 3:], window_shape=piece_shape)
    for area in (a for a_list in slide for a in a_list):
        transparency_mask = piece_arr[:, :, 1] > 120
        mean_diff = np.abs(piece_arr[:, :, 0] - area)[transparency_mask].mean()

        score = 1 - mean_diff / 255
        if score > best_score:
            best_score = score
            best_area = area

    return best_score, best_area


def resize_by_ratio(img, ratio, resample=Image.FLOYDSTEINBERG):
    new_width = round(img.width * ratio)
    new_height = round(img.height * ratio)
    return img.resize((new_width, new_height), resample=resample)
